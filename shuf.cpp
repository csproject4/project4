/* ----------------------------------
 * Program made by:      NAMAN PUJARI 
 * ---------------------------------- */

#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

vector<string> read();
void flush(string& S);
vector<int> numbers;
void query(int Count, bool checkEcho, bool checkUserange);
void inputRange(int count, int low, int high);
void echoLine(int count); 
void headCount(int count);
// for the echo case
vector<string> recieved_words;

static int echo=0, rlow=0, rhigh=0;

int main(int argc, char *argv[]) {
	// define long options
	//static int echo=0, rlow=0, rhigh=0;
	// WAS PREVIOUSLY static size_t count=-1!
	static int count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				while(optind < argc) {
					recieved_words.push_back(argv[optind++]);
				}
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */
	//cout << " USER-RANGE IS " << userange << "\n";
	//if(userange) cout << rlow << "\t" << rhigh << "\n";
	//if(count >= 0) cout << count << "\n";
	//cout << echo;	
	// Create a handle function that sends each query into a suitable function
	//cout << count << "\n";
	query(count ,(bool)echo, userange);
	//headCount(count, (bool)echo, userange);
	return 0;
}

// In any case, -n will be processing outputs from stdin. 
// We may also suppose that it will be choosing two random 
// elements of a vector containing the data sets of values
// that are meant to be shuffled.

// headCount is either compatible with standard stdin, with -e
// or even with -i. Hence we will make these our parameters.

void query(int Count, bool checkEcho, bool checkUserange) {
	// -e and -i cannot coexist
	bool checkCount = Count != -1;
	if(checkUserange && checkEcho) {
		cout << "-e and -i are mutually exclusive!\n";
		return;
	} else if(checkCount && !checkEcho && !checkUserange) headCount(Count); 
	else if(checkCount && !checkEcho && checkUserange) inputRange(Count, rlow, rhigh);
	else if(checkCount && checkEcho && !checkUserange) echoLine(Count);
	else if(!checkCount && checkEcho && !checkUserange) echoLine(Count);
	else if(!checkCount && !checkEcho && checkUserange) inputRange(Count, rlow, rhigh);
	else if(!checkCount && !checkEcho && !checkUserange) headCount(Count);	
	else {}
}

void headCount(int count) {
	// Initialize all the cases headCount can be used with
	vector<string> lines = read();
	if(count == -1) count = lines.size();
	else {}

	srand (time(NULL));
	for(int i = 0; i < count; i++) {
		int random_index = rand() % (lines.size() - i) + i;
		cout << lines[random_index] << "\n";
		std::swap(lines[i], lines[random_index]);
	}
}

void inputRange(int count, int low, int high) {
	bool checkCount = count != -1;
	vector<int> sorted_numbers;
	srand (time(NULL)); // initialize time seed
	for(int i = low; i <= high; i++) {
		sorted_numbers.push_back(i);
	} // now to randomize the data!
	for(int j = 0; j < ((high - low) + 1); j++) {
		int random_index = rand() % (sorted_numbers.size() - j) + j;
		if(!checkCount) cout << sorted_numbers[random_index] << "\n";
		std::swap(sorted_numbers[j], sorted_numbers[random_index]);
	} // In this step we accomodated for PURE -i flag	
	if(checkCount) {
		// The 'sorted' numbers are now randomized so we can take
		// a slice of consecutive numbers from the already randomized
		// vector of integers!
		for(int chosen_index = 0; chosen_index < count; chosen_index++) {
			cout << sorted_numbers[chosen_index] << "\n";
		}
	} else {}
}

void echoLine(int count) {
	bool checkCount = count != -1;
	if(!checkCount) count = recieved_words.size();
	else {}

	srand (time(NULL));
	for(int i = 0; i < count; i++) {
		int random_index = rand() % (recieved_words.size() - i) + i;
		cout << recieved_words[random_index] << "\n";
		std::swap(recieved_words[i], recieved_words[random_index]);
	}
}

vector<string> read() {
	fseek(stdin, 0L, SEEK_END);
	int size = ftell(stdin);
	rewind(stdin);

	char c;
	int char_count = 0;
	string currentLine;
	vector<string> lines;
	while(fread(&c, 1, 1, stdin) != 0) {
		char_count++;
		if(char_count != size) {
			if(c != '\n') currentLine += c;
			else if(c == '\n' || c == '\0') {
				lines.push_back(currentLine);
				flush(currentLine);
			} else {}
		} else if(char_count == size) {
			currentLine+= c;
			lines.push_back(currentLine);
			flush(currentLine);
		} else {}
	} return lines;
}

void flush(string& S) {
	string J;
	S = J;
}
