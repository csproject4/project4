/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: Professor William Skeith's advice on wc. 
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 5 
 */


/* ---------------------------------------------------
 * Program made by:      NAMAN PUJARI AND TANOY SARKAR
 * --------------------------------------------------- */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <iostream>
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

set<string> uWords;
size_t words = 0;
size_t newLine = 0;
size_t char_count = 0; // Always has the terminating \0
size_t maxLenLine = 0;

void read(FILE* f);
void empty(string& S);
void compare(size_t& candidate, size_t& max);

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	read(stdin);
	size_t sum = charonly + linesonly + wordsonly 
					+ uwordsonly + longonly;
	if((bool)linesonly) cout << newLine << " ";
	if((bool)wordsonly) cout << words << " ";
	if((bool)charonly) cout << char_count << " ";
	if((bool)longonly) cout << maxLenLine << " ";
	if((bool)uwordsonly) {
		if(uWords.size() <= 9) {
			cout << " " << uWords.size() << " ";
		} else cout << uWords.size() << " ";
	}

	if(sum == 0) cout << "\t" << newLine << "\t" << words <<
					"\t" << char_count;
	cout << "\n";
	return 0;
}

void read(FILE *f) {
	// determine size of the text file by using the fseek
	// function. This can allow you to properly recognize 
	// the last word in a non new-line terminating text
	// file...
	
	fseek(stdin, 0L, SEEK_END);
	size_t size = ftell(stdin);
	rewind(stdin);

	enum state {ws,nws};
	state State =  ws;
	string currentWord;
	size_t lenLine = 0;
	char c; // The character to read
	while(fread(&c, 1, 1, f) != 0) {
		char_count++;
		if(char_count == 1) {
			if(c == ' ') {
				State = ws; 
				lenLine++;
			} else if(c == '\n') {
				State = ws; 
				newLine++; // add to linecount
				compare(lenLine, maxLenLine); // compares with max length
			} else if(c == '\t') {
				State = ws;
				lenLine += 8;
			} else {
				State = nws; 
				lenLine++;
			}
		} else if(char_count > 1) {
			if(State == ws) {
				if(c == ' ' || c == '\n' || c == '\t') {
					State = ws; 
					if(c == ' ') {
						lenLine++;
					} else if(c == '\n') {
						compare(lenLine, maxLenLine);
						newLine++;
					} else if(c == '\t') {
						lenLine += 8 - (lenLine % 8);
					} else {}
				} else if(c != ' ' && c != '\n' && c != '\t') {
					State = nws; 
					lenLine++; 
					currentWord += c; // word initiated
					if(char_count == size) {
						words++;
						uWords.insert(currentWord);
						empty(currentWord);
					} else {}
				} else {}
			} else if(State == nws) {
				if(c == ' ' || c == '\n' || c =='\t') {
					State = ws;
					words++;
					uWords.insert(currentWord);
					empty(currentWord);
					if(c == ' ') {
						lenLine++;
					} else if(c == '\n') {
						compare(lenLine, maxLenLine);
						newLine++;
					} else if(c == '\t') {
						lenLine += 8 - (lenLine % 8);
					} else {}
				} else if(c != ' ' && c != '\n' && c != '\t') {
					State = nws;
					currentWord += c;
					lenLine++;
					if(char_count == size) {
						words++;
						uWords.insert(currentWord);
						empty(currentWord);
					} else {}
				} else {}
			} else {}
		} else {}
	}
}

void empty(string& S) {
	string J;
	S = J;
}

void compare(size_t& candidate, size_t& max) {
	if(candidate > max) {
		max = candidate;
	} else {}
	candidate = 0;
}

