/* ---------------------------------------------------
 * Program made by:      NAMAN PUJARI AND TANOY SARKAR
 * --------------------------------------------------- */

#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

vector<string> lines;
void flush(string& S);
void read(FILE *f);
void run(bool count, bool dups, bool uniq);

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	read(stdin);
	run((bool)showcount, (bool)dupsonly, (bool)uniqonly);
	return 0;
}

void read(FILE *f) {
	// The difference between this and wc is that
	// we are considering unique lines. So no need 
	// of the schematic to slice words
	//continue;
	char c;
	string currentLine;
	while(fread(&c, 1, 1, f) != 0) {
		if(c != '\n') currentLine += c;
		else if(c == '\n' || c == '\0') {
			lines.push_back(currentLine);
			flush(currentLine);
		} else {}
	}
}

void run(bool count, bool dups, bool uniq) {	
	size_t line_index;
	//sort(line.begin(), lines.end());
	if(!count && dups && uniq) return;
	else if(count && dups && uniq) return;
	else {
		for(line_index = 0; line_index < lines.size(); line_index++) {
			size_t occurences = 1;
			string current = lines[line_index];
			//cout << "\t\t I AM AT " << current << "\n";
			while(current == lines[line_index]) {
				if(line_index == lines.size() - 1 && occurences == 1) {
					if(uniq) {
						if(count) {
							if(occurences >= 10) {							
								cout << "     " << occurences 
									<< " " << current << '\n';
							} else if(occurences < 10) {
								cout << "      " << occurences 
									<< " " << current << '\n';
							} else {}
						} else cout << current << "\n";
					} else {}
					break;
				} else if(line_index <= lines.size() - 1) {
					line_index++;
					if(current == lines[line_index]) {
						occurences++;
						//cout << "\t\t OCCURENCES IS NOW " << occurences << "\n";
					} else if(current != lines[line_index]) {
						if(occurences == 1 && uniq) {
							if(count) {
								if(occurences >= 10) {							
									cout << "     " << occurences 
										<< " " << current << '\n';
								} else if(occurences < 10) {
									cout << "      " << occurences 
										<< " " << current << '\n';
								} else {}
							} else cout << current << "\n";
						} else if(occurences > 1 && dups) {
							if(count) {
								if(occurences >= 10) {							
									cout << "     " << occurences 
										<< " " << current << '\n';
								} else if(occurences < 10) {
									cout << "      " << occurences 
										<< " " << current << '\n';
								} else {}
							} else cout << current << "\n";
						} else {}
						line_index--;
						break;
					} else {}
				} else {}
			}
			if(count && !dups && !uniq) {
				if(occurences >= 10) {
					cout << "     " << occurences << " "<< current << '\n';
				} else cout << "      " << occurences << " "<< current << '\n';
			} else if (!count && !dups && !uniq) cout << current << '\n';
		}
	}
}

void flush(string& S) {
	string J;
	S = J; 
}
