/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: sorry, i lost count
 */

/* -------------------------------
 * Program made by:  MARVIN ESTIME
 * ------------------------------- */

 #include <iostream>
 using std::cin;
 using std::cout;
 using std::endl;
 using std::getline;
 #include <string>   
 using std::string;
 #include <vector>
 using std::vector;
 #include <algorithm>
 using std::sort;
 #include <string.h> // for c-string functions.
 #include <getopt.h> // to parse long arguments.
 
 static const char* usage =
 "Usage: %s [OPTIONS] SET1 [SET2]\n"
 "Limited clone of tr.  Supported options:\n\n"
 "   -c,--complement     Use the complement of SET1.\n"
 "   -d,--delete         Delete characters in SET1 rather than translate.\n"
 "   --help          show this message and exit.\n";
 
 void escape(string& s) {
	 /* NOTE: the normal tr command seems to handle invalid escape
	  * sequences by simply removing the backslash (silently) and
	  * continuing with the translation as if it never appeared. */
	 /* TODO: write me... */
 
	 string output;
	 for(int c = 0; c < s.length(); c++)
	 {
		 if(s[c] == '\\')
		 {
			 if(c < s.length() - 1)
			 {
				 char nextChar = s[c + 1];
				 if(nextChar == 'n' )
				 {
					 output += '\n';
					 c++;
				 }
				 else if(nextChar == 't' )
				 {
					 output += '\t';
					 c++;
				 }
				 else if(nextChar == '\\')
				 {
					 output += '\\';
					 c++;
				 }
			 }
		 }
		 else
		 {
			 output += s[c];
		 }
	 }
	 s = output;
 }
 
 vector <char> createVector(string s, int complement) //transforms string into appropriate
 {                                                    // char vector
	 vector <char> output;
	 if(complement) //include all characters not found in the string
	 {
		 for(int c = 0; c < 256; c++)
		 {
			 bool occurence = false;
			 for(int d = 0; d < s.length(); d++)
			 {
				 if((char)c == s[d])
				 {
					 occurence = true;
				 }
			 }
			 if(!occurence)
			 {
				 output.push_back((char)c );
			 }
		 }
	 }
	 else //creat a direct vector representation of the string
	 {
		 for(int c = 0; c < s.length(); c++)
		 {
			 output.push_back(s[c]);
		 }
	 }
	 return output;
 }
 
 string translate (vector <char> firstSet, vector <char> secondSet, string input, int delValue)
 {
	 string output;
	 int position;
	 bool occurence;
	 for(int c = 0; c < input.length(); c++)
	 {
		 occurence = false;
		 for(int d = 0; d < firstSet.size(); d++)
		 {
			 if(input[c] == firstSet[d])
			 {
				 position = d;
				 occurence = true;
			 }
		 }
		 if(!delValue && occurence ) //else delValue is true and the character will be excluded
		 {
			 if(position < secondSet.size())
			 {
				 output += secondSet[position];
			 }
			 else
			 {
				 output += secondSet[secondSet.size() - 1];
			 }
		 }
		 else if(!occurence)
		 {
			 output += input[c];
		 }   
	 }
 
	 return output;
 }
 
 int main(int argc, char *argv[])
 {
	 // define long options
	 static int comp=0, del=0;
	 static struct option long_opts[] = {
		 {"complement",      no_argument,   0, 'c'},
		 {"delete",          no_argument,   0, 'd'},
		 {"help",            no_argument,   0, 'h'},
		 {0,0,0,0}
	 };
	 // process options:
	 char c;
	 int opt_index = 0;
	 while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		 switch (c) {
			 case 'c':
				 comp = 1;
				 break;
			 case 'd':
				 del = 1;
				 break;
			 case 'h':
				 printf(usage,argv[0]);
				 return 0;
			 case '?':
				 printf(usage,argv[0]);
				 return 1;
		 }
	 }
	 if (del) {
		 if (optind != argc-1) {
			 fprintf(stderr, "wrong number of arguments.\n");
			 return 1;
		 }
	 } else if (optind != argc-2) {
		 fprintf(stderr,
				 "Exactly two strings must be given when translating.\n");
		 return 1;
	 }
	 string s1 = argv[optind++];
	 string s2 = (optind < argc)?argv[optind]:"";
	 /* process any escape characters: */
	 escape(s1);
	 escape(s2);
	
	 vector <char> firstCharList = createVector(s1,comp);
	 vector <char> secondCharList = createVector(s2,0);
	
	 string step, input, output; // if c, comp = 1; if d, del = 1;
	
	 while(getline(cin, step)) 
	 {
		 input += (step + '\n');
	 }
	 input = input.substr(0,input.size() - 1);
	 
	// translate (vector <char> firstSet, vector <char> secondSet, string input, int delValue)
	 output = translate(firstCharList, secondCharList, input, del);
	 cout << output ;
 
	 /* TODO: finish this... */
 
	 return 0;
 }
