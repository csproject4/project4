/* ---------------------------------------
 * Program made by:          JEFFREY ZHENG
 * --------------------------------------- */

#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
#include <vector>
using std::vector;
using std::endl;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}


	//set<string,igncaseComp> S; //multisets op

	/* TODO: write me... */
	//descending = 1, if descending -r
	//ignorecase = 1, if ignoring case -f 
	//unique = 1, if removing duplicates -u

	string output, lines, currentString, nextString, tempString;
	int currentCharValue, nextCharValue;
	vector <string> vectorInputs; 

	if(unique)
	{
		set <string> inputs;
		while(getline(cin, lines))
		{
			inputs.insert(lines);
		}
		for(set <string>::iterator i = inputs.begin(); i != inputs.end(); i++)
		{
			vectorInputs.push_back(*i);
		}
	}
	else
	{
		while(getline(cin, lines))
		{
			vectorInputs.push_back(lines);
		}
	}

	for(int c = 0; c < vectorInputs.size() - 1; c++)
	{
		for(int d = c + 1; d < vectorInputs.size(); d++ )
		{
			currentString = vectorInputs[c];
			nextString = vectorInputs[d];
			for(int e = 0; e < currentString.length(); e++)
			{
				currentCharValue = currentString[e];
				nextCharValue = nextString[e];
				
				// a -> z, 97 -> 122	
				if(ignorecase)
				{
					if(currentCharValue >= 97 && currentCharValue <= 122)
					{
						currentCharValue -= 32;
					}
					if(nextCharValue >= 97 && nextCharValue <= 122)
					{
						nextCharValue -= 32;
					}
				}
				if(currentCharValue < nextCharValue)
				{
					break;
				}
				if(currentCharValue > nextCharValue)
				{
					tempString = vectorInputs[c];
					vectorInputs[c] = vectorInputs[d];
					vectorInputs[d] = tempString;
					break;
				}
			}
		}
	} //end of vector sort

	if(descending)
	{
		for(int c = vectorInputs.size() - 1; c >= 0; c--)
		{
			output += (vectorInputs[c] + '\n');
		}
	}
	else
	{
		for(int c = 0; c < vectorInputs.size(); c++)
		{
			output += (vectorInputs[c] + '\n');
		}
	}
	cout << output;
	return 0;
}
	



/*


	if(unique)				 
	{ 
		if(ignorecase)
		{
			vector <string> vectorInputs;
			int currentCharValue, nextCharValue;
			string currentString, nextString, tempString;
			set <string> inputs;

			while(getline(cin, lines))
			{
				inputs.insert(lines);
			}
			for(set <string>::iterator i = inputs.begin(); i != inputs.end(); i++)
			{
				vectorInputs.push_back(*i);
			}
			
			for(int c = 0; c < vectorInputs.size() - 1; c++)
			{
				for(int d = c + 1; d < vectorInputs.size(); d++ )
				{
					currentString = vectorInputs[c];
					nextString = vectorInputs[d];
					for(int e = 0; e < currentString.length(); e++)
					{
						// a -> z, 97 -> 122
						currentCharValue = currentString[e];
						nextCharValue = nextString[e];
						if(currentCharValue >= 97 && currentCharValue <= 122)
						{
							currentCharValue -= 32;
						}
						if(nextCharValue >= 97 && nextCharValue <= 122)
						{
							nextCharValue -= 32;
						}
						if(currentCharValue < nextCharValue)
						{
							break;
						}
						if(currentCharValue > nextCharValue)
						{
							tempString = vectorInputs[c];
							vectorInputs[c] = vectorInputs[d];
							vectorInputs[d] = tempString;
							break;
						}
					}
				}

			}
			 //end of vector sorting
	
			if(!descending)
			{
				for(int c = 0; c < vectorInputs.size(); c++)
				{
						output += (vecotrInputs[c] + '\n') ;			
				}
			}
			else
			{	
				for(int c = vectorInputs.size() - 1; c >= 0; c--)
				{
					output += (vectorInputs[c]);
				}
			}
		} //end of ignorecase (within unique)
		else
		{
			set <string> input;
			while(getline(cin, lines))
			{
				input.insert(lines);
			}

			if(!descending)
			{
				for(set <string>::iterator i = input.begin(); i != input.end(); i++)
				{
						output += (*i + '\n') ;			
				}
			}
			else
			{
				for(set <string>::iterator i = input.begin(); i != input.end(); i++)
				{
					reversed.push_back(*i);
				}
				for(int c = 1; c <= reversed.size(); c++)
				{
					output += (reversed[reversed.size() - c] + '\n');
				}
			}
		}
	}
	else
	{
		if(ignorecase)
		{
			multiset <string, igncaseComp> input;
			while(getline(cin, lines))
			{
				input.insert(lines);
			}
	
			if(!descending)
			{
				for(multiset <string, igncaseComp>::iterator i = input.begin(); i != input.end(); i++)
				{
						output += (*i + '\n') ;			
				}
			}
			else
			{
				for(multiset <string, igncaseComp>::iterator i = input.begin(); i != input.end(); i++)
				{
					reversed.push_back(*i);
				}
				for(int c = 1; c <= reversed.size(); c++)
				{
					output += (reversed[reversed.size() - c] + '\n');
				}
			}
		}
		else
		{
			multiset <string> input;
			while(getline(cin, lines))
			{
				input.insert(lines);
			}

			if(!descending)
			{
				for(multiset <string>::iterator i = input.begin(); i != input.end(); i++)
				{
						output += (*i + '\n') ;			
				}
			}
			else
			{
				for(multiset <string>::iterator i = input.begin(); i != input.end(); i++)
				{
					reversed.push_back(*i);
				}
				for(int c = 1; c <= reversed.size(); c++)
				{
					output += (reversed[reversed.size() - c] + '\n');
				}
			}
		}
	}

	cout << output;
	
	return 0;
}
*/
